// JSON

/* 
Instroduction to JSON
Javascript Object Notation

data format used by applications to store and transport data to one another.

not limited to javascript based applications and can also be used in other programming languages

saved with file extension of .json


*/

/* 
    Create a JS object with the following properties

        city
        province
        country
*/
 


// Difference

/* 
1. Quotation marks
    JS objectses only has tha value inserted inside quotationmarks
    JSON  has the quotation marks for both key and the value
2. 
    JS Objects - exclusive to JS, other programming languages cannot use js object files.
    JSON - 



*/


// JSON 
/* 
 used for serializing or deserializing different data types into byte
    serialization - process of coverting into series of bytes for easier transmission or transfer of information

    - byte - binary lanuage
*/
/* let address = {
    'city' : 'dasma',
    'province' : 'Cavite',
    'country' : 'philippines'
};


console.log(address);

// JSON Arrays
let cities = [
    {
        'city' : 'dasma',
        'province' : 'Cavite',
        'country' : 'philippines'
    
    },
    {
        'city' : 'Bacoor',
        'province' : 'Cavite',
        'country' : 'philippines'
    
    },
    {
        'city' : 'new york',
        'province' : 'new york',
        'country' : 'USA'
    
    }
];
console.log(cities)
*/

//JSON METHODS 
/* 
    JSON object contains methods for parsing and converting data into stringified JSON

    Stringified JSON - json object converted intro string to be used in other functions of the language esp. javscript based applciations (serialize)
*/

let batches = [
    {
        "batchName" : "batchX"
    },
    {
        "batchName" : "Batch Y"
    }
]

console.log("result from console.log");
console.log(batches);


console.log("result from stringify method");
// stringify - used to convert JSON objects into json (string)
console.log(JSON.stringify(batches));

let data = JSON.stringify({
    name : "john",
    age : 31,
    address : {
        city: "Manila",
        country: "Philippines"
    }
})

console.log(data)

/* 
Assignment
    create userDetails variable that will contain JS object with the following properties

        fname = prompt
        lname = prompt
        age = prompt
        address:{
            city = prompt
            country = prompt
            zipCode = prompt
        log in the console the converted JSON data type
        }
*/

/* 

let userDetails = JSON.stringify({
    fname:prompt('Please input your name'),
    lname:prompt('Please input your last name'),
    age:prompt('Please input your age'),
    address:{
        city : prompt('city'),
        country : prompt('Country'),
        zipCode : prompt('Zipcode')
    }
}
); 

*/

// console.log(userDetails);

// COnverting of Stringified JSON ito JS objects
/* 
    Parse method - convertinog json data into js objects
    information is commonly sent to application in Stringified JSON ; then converted back into objects; this happens both for sending information to a backend app, such as databases and back to frontend app such as the webpages.

    upon receiving the data, JSON text can be converted into JS/JSON object with parse method



*/

let batchesJSON = `[
    {
        "batchName" : "batchX"
    },
    {
        "batchName" : "Batch Y"
    }
]`;

console.log(batchesJSON);
console.log("Result from parse method");
console.log(JSON.parse(batchesJSON));


let stringifiedData = `{
    "name" : "john",
    "age" : 31,
    "address" : {
        "city": "Manila",
        "country": "Philippines"
    }
}`;
  
// How do we convert the variable above into a JS obbject

console.log(JSON.parse(stringifiedData))









 





















